/************************************************************************************
 * This file is part of the driver for interfacing with the CubeACP
 ***********************************************************************************/

#ifndef INC_CUBEACP_UI_H_
#define INC_CUBEACP_UI_H_

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "cubeacp.h"

/******************************************************************************
 * Interface functions - needs to be implemented by user
 *****************************************************************************/
/***************************************************************************//**
 * Opens a file stream - mirror function of fopen
 *
 * @param[in] file
 *   C string containing the name of the file to be opened.
 * @param[in] mode
 *   C string containing a file access mode. See use of fopen
 * @return
 *   If the file is successfully opened, the function returns a pointer to a FILE object that can be used to identify the stream
 *
 ******************************************************************************/
FILE *Cube_fopen( const char *file, const char *mode );

/***************************************************************************//**
 * write bytes to a file stream - mirror function of fwrite
 *
 * @param[in] buffer
 *   The pointer to the array of elements to be written.
 * @param[in] xSize
 *   The size in bytes of each element to be written
 * @param[in] items
 *   The number of elements
 * @param[in] stream
 *   Pointer to a FILE object that specifies an output stream
 * @return
 *   Returns the total number of elements successfully returned
 *
 ******************************************************************************/
uint16_t Cube_fwrite( const void *buffer, uint16_t xSize, uint16_t items, FILE * stream );

/***************************************************************************//**
 * Closes a file stream - mirror function of fclose
 *
 * @param[in] stream
 *   Pointer to a FILE object that specifies the stream to be closed.
 * @return
 *   If the stream is successfully closed, a zero value is returned.
 *
 ******************************************************************************/
int Cube_fclose( FILE *stream );

/***************************************************************************//**
 * Writes the C string to a debug stream, useful for debugging.  Not functional, only used for debugging.
 *
 * @param[in] msg
 *   C string that contains the text to be written to a debug stream.
 *
 ******************************************************************************/
void debug_printf(char* msg, ...);

/***************************************************************************//**
 * Returns the current millisecond tick, used for timeout of operations
 *
 * @return
 *   Current millisecond tick counter
 *
 ******************************************************************************/
uint16_t getTick(void);

/***************************************************************************//**
 * Delay the current operations
 *
 * @param[in] delay
 *   The number of millisecond delay
 *
 ******************************************************************************/
void Delay(uint32_t delay);

/***************************************************************************//**
 * Transmits a telecommand message to the CubeACP
 *
 * @param[in] buffer1
 *   The pointer to the buffer containing the data bytes to be sent to the CubeACP
 * @param[in] buffer1Size
 *   The number of bytes to be sent of the buffer
 *
 ******************************************************************************/
bool MasterTelecommand(uint8_t *buffer1, uint16_t buffer1Size);

/***************************************************************************//**
 * Transmits a telemetry request and receives a reply from the CubeACP
 *
 * @param[in] buffer1
 *   The pointer to the buffer containing the data bytes to be sent to the CubeACP
 * @param[in] buffer1Size
 *   The number of bytes to be sent of the buffer
 * @param[in] buffer2
 *   The pointer to the buffer which is populated by the data bytes returned by the CubeACP
 * @param[in] buffer2Size
 *   The number of bytes to be returned by the CubeACP
 *
 ******************************************************************************/
bool MasterTelemetry(uint8_t *buffer1, uint16_t buffer1Size,uint8_t *buffer2, uint16_t buffer2Size);

/******************************************************************************
 * File Download Functions
 *****************************************************************************/

/***************************************************************************//**
 * Downloads the file information of all the files currently on the SD-card.
 *
 * @param[in] link
 *   A pointer which will point to the first address of the file list
 * @return
 *   Returns a uint8_t which is the number of files in the obtained file list
 *
 ******************************************************************************/
uint8_t CubeACP_ReadFileList(CUBEACP_FileInfo_t** link);

/***************************************************************************//**
 * Downloads a complete file and saves the file on the local file system
 *
 * @param[in] filename
 *   A null character ending string containing the filename of the file to be created
 * @param[in] targetFile
 *   The structure containing the description of the file to be downloaded
 * @return
 *   Returns a bool which will be true if the file download was successful
 *
 ******************************************************************************/
bool CubeACP_DownloadFile(char* fileName, CUBEACP_FileInfo_t* targetFile);

/***************************************************************************//**
 * Downloads a single block of a specific file
 *
 * @param[in] targetFile
 *   The structure containing the description of the file to be downloaded
 * @param[in] fileOffset
 *   The byte offset within the file where the downloaded block will start
 * @param[in] numBytes
 *   A pointer to a uint16_t which will populated with the number of bytes which were downloaded
 * @return
 *   Returns a bool which will be true if the file block download was successful
 *
 ******************************************************************************/
bool CubeACP_DownloadBlock(CUBEACP_FileInfo_t* targetFile, uint16_t fileOffset, uint16_t* numBytes);


#endif /* INC_CUBEACP_UI_H_ */
