
#include "cubeacp_ui.h"
#include "cubeacp.h"
#include "bsp_i2c.h"
#include "bsp_uart.h"
#include "bsp_time.h"

//#define TEST_HOLEMAP

#define maxFileList 10

CUBEACP_FileInfo_t FileList [maxFileList];

static uint8_t uploadMessageLen = 20;
static uint16_t uploadBurstLength = 1024;
static uint8_t uploadHoleMapTlmLen = 16;

static uint8_t downloadMessageLen = 20;
static uint16_t downloadBurstLength = 1024;
static uint8_t downloadHoleMapTcLen = 16;
I2C_TransferReturn_TypeDef i2cResult;
uint8_t Block [20480];
uint8_t holemap[128];
uint8_t holeMapPart[16];
bool DownloadCancel = false;

static bool TcmdProcessed(uint8_t id);
static uint16_t CRC_Calc(uint8_t *start, uint32_t len);

uint8_t CubeACP_ReadFileList(CUBEACP_FileInfo_t** link)
{
	uint8_t counter = 0;
	uint32_t tick, tock;
	uint8_t i;
	bool end = false;
	uint8_t i2cTxBuffer[500];
	uint8_t i2cRxBuffer[500];
	uint32_t i2cTxLen;

	// clear current FileList
	memset((uint8_t*)FileList, 0, maxFileList*sizeof(CUBEACP_FileInfo_t));

	// reset file pointer
	i2cTxLen = CUBEACP_ResetFileListPtrCmd(i2cTxBuffer);
	i2cResult = MasterTelecommand(i2cTxBuffer, i2cTxLen);

	TcmdProcessed(i2cTxBuffer[0]);

	Delay(100);

	tick = getTick();
	tock = getTick();
	do
	{
		// request file info
		i2cTxLen = CUBEACP_createTelemetryRequest(i2cTxBuffer, CubeAcp_FileInfo);
		i2cResult = MasterTelemetry(i2cTxBuffer, 1, i2cRxBuffer, i2cTxLen);
		CUBEACP_FileInfoTlm(i2cRxBuffer, &FileList[counter]);

		if(FileList[counter].busyUpdating == false && i2cResult)
		{
			if((FileList[counter].fileType == 0) && (FileList[counter].fileCtr == 0) && (FileList[counter].size == 0) && (FileList[counter].checksum == 0))
			{
				end = true;
			}
			else
			{
				// valid file info received, reset timeout counter
				tick = getTick();

				// advance file pointer
				i2cTxLen = CUBEACP_AdvanceFileListPtrCmd(i2cTxBuffer);
				i2cResult = MasterTelecommand(i2cTxBuffer, i2cTxLen);


				TcmdProcessed(i2cTxBuffer[0]);

				// increment number of valid file info received
				counter++;
			}
		}
		else
		{
			Delay(20);
		}

		tock = getTick();
	}while((end == false) && (tock-tick < 1000));

	debug_printf("Total files: %d", counter);

	for(i = 0;i<counter;i++)
	{
		debug_printf("FileType %d\tFileCounter %d\tFileSize %d\tCRC %d", FileList[i].fileType, FileList[i].fileCtr, FileList[i].size, FileList[i].checksum);
	}

	if((tock - tick >= 1000))
	{
		debug_printf("Reading file list timeout");
	}

	*link = FileList;
	return counter;
}

bool CubeACP_DownloadFile(char* fileName, CUBEACP_FileInfo_t* targetFile)
{
	uint8_t i = 0;
	uint16_t numBytes;
	bool success = true;
	FILE* handle = NULL;

	handle = Cube_fopen( fileName, "w" );
	if(handle == NULL)
	{
		success  = false;
	}
	else
	{
		do
		{
			numBytes = 0;
			success = CubeACP_DownloadBlock(targetFile, i*downloadBurstLength*downloadMessageLen, &numBytes);

			if(success)
			{
				debug_printf("Writing block %d", i);
				Cube_fwrite(Block, sizeof(uint8_t), numBytes, handle);
			}

			i++;
		}while((i<targetFile->size/(downloadBurstLength*downloadMessageLen)) && success);

		if(Cube_fclose(handle) != 0)
		{
			success = false;
		}
	}

	return success;
}

bool CubeACP_DownloadBlock(CUBEACP_FileInfo_t* targetFile, uint16_t fileOffset, uint16_t* numBytes)
{
	CUBEACP_DownloadBlockReady_t downloadBlockReady;
	CUBEACP_FileDownload_t filePacket;

	bool blockdone;
	bool newBytes;
	bool cont;
	bool noreply;
	bool firstBurst = true;
	uint8_t retrycount = 0;
	uint16_t counter;
	uint16_t counterRef;
	uint16_t crcRX;
	uint16_t i;
	uint16_t packetsLost = 0;
	uint32_t tick, tock;

	uint8_t i2cTxBuffer[500];
	uint8_t i2cRxBuffer[500];
	uint32_t i2cTxLen;

	uint16_t holeMapLen = downloadBurstLength / 8;
	uint16_t downloadHoleMapNumTcs = holeMapLen / downloadHoleMapTcLen;

	// Clear holemap
	memset(holemap,0,128);
	memset(Block,0,20480);

	*numBytes = 0;

	debug_printf("Download Block");

	// Load new block into memory
	i2cTxLen =  CUBEACP_LoadDownloadBlockCmd(i2cTxBuffer, targetFile->fileType, targetFile->fileCtr, fileOffset, (uint16_t)(downloadBurstLength * downloadMessageLen));
	i2cResult = MasterTelecommand(i2cTxBuffer, i2cTxLen);

	Delay(1000);

	debug_printf("Load Block Command");

	if(!TcmdProcessed(i2cTxBuffer[0]))
	{
		return false;
	}

	// Wait until block has been loaded successful
	tick = getTick();
	tock = getTick();
	do
	{
		i2cTxLen = CUBEACP_createTelemetryRequest(i2cTxBuffer, CubeAcp_DownloadBlockReady);
		i2cResult = MasterTelemetry(i2cTxBuffer, 1, i2cRxBuffer, i2cTxLen);
		CUBEACP_DownloadBlockReadyTlm(i2cRxBuffer, &downloadBlockReady);

		// check if block is loaded, else give a short time before next poll
		if((!downloadBlockReady.ready) || (i2cResult != true))
		{
			Delay(20);
		}

		tock = getTick();
	}while((!downloadBlockReady.ready || (i2cResult != true)) && (tock - tick < 1000));

	// check timeout
	if((tock - tick >= 1000))
	{
		debug_printf("Download Block Ready Error");
		return false;
	}

	blockdone = false;
	while(!blockdone && !DownloadCancel)
	{
		noreply = true;
		retrycount = 0;
		while(noreply && (retrycount < 3))
		{
			// initialise download burst
			i2cTxLen =  CUBEACP_DownloadBurstCmd(i2cTxBuffer, downloadMessageLen, firstBurst);
			i2cResult = MasterTelecommand(i2cTxBuffer, i2cTxLen);

			if(TcmdProcessed(i2cTxBuffer[0]))
			{
				// set reference for number of file packets to request
				if(firstBurst)
				{
					counterRef = (downloadBlockReady.length+19)/20;	// 1023
				}
				else
				{
					counterRef = packetsLost;	// number packets i expect to have lost
				}
				firstBurst = false;

				// ... get actual data
				cont = false;
				counter = 0;
				do
				{
					cont = false;
					if(counter < counterRef)
					{
						i2cTxLen = CUBEACP_createTelemetryRequest(i2cTxBuffer, CubeAcp_FileDownload);
						i2cResult = MasterTelemetry(i2cTxBuffer, 1, i2cRxBuffer, i2cTxLen);
						CUBEACP_FileDownloadTlm(i2cRxBuffer, &filePacket);

						//debug_printf("%d", filePacket.packetNo);

						newBytes = true;

						counter++;

						Delay(20);
					}
					else
					{
						newBytes = false;
					}

					if(newBytes)
					{
#ifdef TEST_HOLEMAP
						if(i2cResult == true && ((rand() % 100) > 5))
						{
							memcpy(Block+filePacket.packetNo*20, filePacket.fileBytes, 20);

							holemap[filePacket.packetNo >> 3] = (uint8_t)(holemap[filePacket.packetNo >> 3] | (1 << (filePacket.packetNo & 0x07)));
						}
#else
						if(i2cResult == true)
						{
							memcpy(Block+filePacket.packetNo*20, filePacket.fileBytes, 20);

							holemap[filePacket.packetNo >> 3] = (uint8_t)(holemap[filePacket.packetNo >> 3] | (1 << (filePacket.packetNo & 0x07)));
						}
#endif

						cont = true;
						noreply = false;
					}
				}while(cont);
			}
			else
			{
				noreply = true;
			}

			if(noreply)
			{
				retrycount++;
			}
		}

		if(retrycount>=3)
		{
			// unable to start downloading process
			debug_printf("Unable to start download process successfully");
			return false;
		}

		// check holemap for gaps
		blockdone = true;
		packetsLost = 0;
		// test holemap for any errors
		for (i = 0; i < downloadBurstLength; i++)
		{
			if (i * downloadMessageLen >= downloadBlockReady.length)
				break;

			if ((holemap[i >> 3] & (1 << (i & 0x07))) == 0)
			{
				blockdone = false;
				packetsLost++;
			}
		}
		debug_printf("No Packets Lost: %d", packetsLost);

		// calculate CRC
		crcRX = CRC_Calc(Block, downloadBlockReady.length);
		debug_printf("Reference: %d Calculated: %d", downloadBlockReady.checksum, crcRX);
		if((downloadBlockReady.checksum != crcRX) && (packetsLost == 0))
		{
			// if CRC fails and no packets are reported to missing then entire block must be reinitialized
			debug_printf("Byte errors in block");
			return false;
		}

		// send holemap telecommands if block is not complete
		if(!blockdone)
		{
			debug_printf("Sending holemap back");
			for (i = 0; i < downloadHoleMapNumTcs; i++)
			{
				memcpy(holeMapPart, holemap+i * downloadHoleMapTcLen, downloadHoleMapTcLen);
				if(i==0)
				{
					i2cTxLen = CUBEACP_HoleMap1Cmd(i2cTxBuffer, holeMapPart);
					i2cResult = MasterTelecommand(i2cTxBuffer, i2cTxLen);
					TcmdProcessed(i2cTxBuffer[0]);
				}
				else if(i==1)
				{
					i2cTxLen = CUBEACP_HoleMap2Cmd(i2cTxBuffer, holeMapPart);
					i2cResult = MasterTelecommand(i2cTxBuffer, i2cTxLen);
					TcmdProcessed(i2cTxBuffer[0]);
				}
				else if(i==2)
				{
					i2cTxLen = CUBEACP_HoleMap3Cmd(i2cTxBuffer, holeMapPart);
					i2cResult = MasterTelecommand(i2cTxBuffer, i2cTxLen);
					TcmdProcessed(i2cTxBuffer[0]);
				}
				else if(i==3)
				{
					i2cTxLen = CUBEACP_HoleMap4Cmd(i2cTxBuffer, holeMapPart);
					i2cResult = MasterTelecommand(i2cTxBuffer, i2cTxLen);
					TcmdProcessed(i2cTxBuffer[0]);
				}
				else if(i==4)
				{
					i2cTxLen = CUBEACP_HoleMap5Cmd(i2cTxBuffer, holeMapPart);
					i2cResult = MasterTelecommand(i2cTxBuffer, i2cTxLen);
					TcmdProcessed(i2cTxBuffer[0]);
				}
				else if(i==5)
				{
					i2cTxLen = CUBEACP_HoleMap6Cmd(i2cTxBuffer, holeMapPart);
					i2cResult = MasterTelecommand(i2cTxBuffer, i2cTxLen);
					TcmdProcessed(i2cTxBuffer[0]);
				}
				else if(i==6)
				{
					i2cTxLen = CUBEACP_HoleMap7Cmd(i2cTxBuffer, holeMapPart);
					i2cResult = MasterTelecommand(i2cTxBuffer, i2cTxLen);
					TcmdProcessed(i2cTxBuffer[0]);
				}
				else if(i==7)
				{
					i2cTxLen = CUBEACP_HoleMap8Cmd(i2cTxBuffer, holeMapPart);
					i2cResult = MasterTelecommand(i2cTxBuffer, i2cTxLen);
					TcmdProcessed(i2cTxBuffer[0]);
				}
			}
		}
	}

	*numBytes = downloadBlockReady.length;
	return blockdone;
}

static bool TcmdProcessed(uint8_t id)
{
	CUBEACP_TelecommandAcknowledge_t tcmAck;
	uint32_t tick, tock;
	uint8_t i2cTxBuffer[500];
	uint8_t i2cRxBuffer[500];
	uint32_t i2cTxLen;

	tick = getTick();
	tock = getTick();
	do
	{
		i2cTxLen = CUBEACP_createTelemetryRequest(i2cTxBuffer, CubeAcp_TelecommandAcknowledge);
		i2cResult = MasterTelemetry(i2cTxBuffer, 1, i2cRxBuffer, i2cTxLen);
		CUBEACP_TelecommandAcknowledgeTlm(i2cRxBuffer, &tcmAck);

		if((!tcmAck.processedFlag) && (tcmAck.lastTCID != id))
		{
			Delay(20);
		}

		tock = getTick();
	}while((!tcmAck.processedFlag) && (tcmAck.lastTCID != id) && (tock-tick < 1000));

	if((tock - tick >= 1000))
	{
		debug_printf("Telecommand acknowledge timeout");
		return false;
	}
	else
	{
		return true;
	}
}

static uint16_t CRC_Calc(uint8_t *start, uint32_t len)
{
	uint16_t crc = 0;
	uint8_t  *data;
	uint8_t  *end = start + len;

	for (data = start; data < end; data++)
	{
		crc  = (crc >> 8) | (crc << 8);
		crc ^= *data;
		crc ^= (crc & 0xff) >> 4;
		crc ^= crc << 12;
		crc ^= (crc & 0xff) << 5;
	}
	return crc;
}
